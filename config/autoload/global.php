<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

return array(
    'smarty_settings' => array(
        'compile_dir' => __DIR__ . '/../../data/smarty/templates_c',
        'cache_dir' => __DIR__ . '/../../data/smarty/cache',
        'caching' => false,
        'cache_lifetime' => 1 * 60 * 60,
        'compile_check' => true,
        'plugins_dir' => array(
            __DIR__ . '/../../vendor/Smarty/lib/plugins',
            __DIR__ . '/../../vendor/Smarty/src/Plugins',
        ),
    )
);
