<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
return array(
    'search' => [
        'strategy_name' => 'sphinx',
        'params' => [
            'query_options' => [
                'ranker' => 'sph04',
                'max_matches' => 20
            ],
            'connection' => [
                'driver' => 'mysqli',
                'host' => '127.0.0.1',
                'dbname' => '',
                'port' => '9306',
                'username' => 'root',
                'password' => ''
            ]
        ]
    ],
    'router' => array(
        'routes' => array(
            'search' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/search[/][:action][/]',
                    'defaults' => array(
                        'controller' => 'BNSearch\Controller\Index',
                        'action' => 'index',
                    ),
                ),
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'BNSearch\Controller\Index' => 'BNSearch\Controller\IndexController'
        ),
    ),
    'service_manager' => array(
        'aliases' => [
            'SearchService' => '\BNSearch\Service\SearchService',
            'SearchServiceFactory' => '\BNSearch\Service\Factory',
            'SearchResultSet' => '\BNSearch\Entity\SearchResultSet',
            'SearchResultItem' => '\BNSearch\Entity\ResultItem'
        ],
        'factories' => [
            '\BNSearch\Service\SearchService' => function($sm) {
                $config = $sm->get('Config')['search'];
                /* @var $strategyFactory \BnSearch\Service\Factory */
                $strategyFactory = $sm->get('SearchServiceFactory');
                /* @var $strategy \BNSearch\Service\Strategy\AbstractStrategy */
                $strategy = $strategyFactory->searchStrategy($config['strategy_name']);
                $strategy->setParams($config['params']);
                $strategy->events()->attach(\BNSearch\Service\Strategy\AbstractStrategy::EVENT_SEARCH_PRE, $strategy->preSearch());
                $strategy->events()->attach(\BNSearch\Service\Strategy\AbstractStrategy::EVENT_SEARCH_POST, $strategy->postSearch());
                $service = new \BNSearch\Service\SearchService;
                $service->setSearchStrategy($strategy);
                return $service;
            },
            '\BNSearch\Service\Factory' => function($sm) {
                return new \BNSearch\Service\Factory;
            },
            'SphinxSearchStrategy' => function($sm) {
                return new \BNSearch\Service\Strategy\SphinxStrategy;
            },
            '\BNSearch\Entity\SearchResultSet' => function($sm) {
                return new \BNSearch\Entity\SearchResultSet;
            },
            '\BNSearch\Entity\ResultItem' => function($sm) {
                return new \BNSearch\Entity\ResultItem;
            }
        ],
        'shared' => [
            '\BNSearch\Entity\ResultItem' => false,
            '\BNSearch\Entity\SearchResultSet' => false
        ]
    )
);
