<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace BNSearch\Controller;

use BN\Mvc\Controller\ActionController;
use Zend\View\Model\ViewModel;

class IndexController extends ActionController
{
    
    public function indexAction()
    {
        $query = $this->params()->fromQuery('q');
        $result = $this->getServiceLocator()->get('SearchService')->search($query, 'bnProducts');
        $products = [];
        $totalCount = $result->count();
        if ($result->count() > 0) {
            /*@var $item \BNSearch\Entity\AbstractSearchResultItem */
            foreach ($result as $item) {
                $products[] = [
                    'id' => $item->getId(),
                    'text' => $item->getName()
                ];
            }
        }
        return $this->getResponse()->setContent(json_encode(array(
            "total_items" => $totalCount,
            "page" => 1,
            "total_pages" => 1,
            "page_size" => 20,
            'products' => $products
        )));
    }
}
