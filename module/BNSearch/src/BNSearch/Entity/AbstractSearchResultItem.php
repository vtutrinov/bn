<?php
namespace BNSearch\Entity;
/**
 * Description of AbstractSearchResultItem
 *
 * @author slava
 */
class AbstractSearchResultItem {
    
    protected $id = null;
    protected $name = null;
    
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }
    
}

?>
