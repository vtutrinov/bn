<?php
namespace BnSearch\Service;

use BN\ServiceManager\ServiceLocatorAwareService;

class Factory extends ServiceLocatorAwareService {

    public function searchStrategy($strategyName) {
        $fullName = ucfirst($strategyName)."SearchStrategy";
        if ($this->getServiceLocator()->has($fullName)) {
            return $this->getServiceLocator()->get($fullName);
        }
        throw new \BNSearch\Exception\InvalidArgument('Search strategy '.$fullName.' is not defined');
    }
    
    public function newResultSet() {
        return $this->getServiceLocator()->get('SearchResultSet');
    }
    
    public function newResultItem() {
        return $this->getServiceLocator()->get('SearchResultItem');
    }

}

?>
