<?php
namespace BNSearch\Service;

use BN\ServiceManager\ServiceLocatorAwareService;

/**
 * Description of SearchService
 *
 * @author slava
 */
class SearchService extends ServiceLocatorAwareService {
    
    /**
     *
     * @var \BNSearch\Service\Strategy\SearchStrategyBehavior 
     */
    protected $searchStrategy = null;
    
    /**
     * 
     * @return \BNSearch\Service\Strategy\SearchStrategyBehavior
     */
    public function getSearchStrategy() {
        return $this->searchStrategy;
    }

    /**
     * 
     * @param \BNSearch\Service\Strategy\SearchStrategyBehavior $searchStrategy
     */
    public function setSearchStrategy(\BNSearch\Service\Strategy\SearchStrategyBehavior $searchStrategy) {
        $this->searchStrategy = $searchStrategy;
    }
    
    public function search($query, $index) {
        return $this->getSearchStrategy()->search($query, $index);
    }
    
}

?>
