<?php

namespace BNSearch\Service\Strategy;

use BN\ServiceManager\ServiceLocatorAwareService;
use Zend\EventManager\EventManagerAwareInterface;
use BN\EventManager\ProvidesEvents;

abstract class AbstractStrategy extends ServiceLocatorAwareService implements SearchStrategyBehavior, EventManagerAwareInterface {
    use ProvidesEvents;
    
    const EVENT_SEARCH_PRE = 'search.pre';
    const EVENT_SEARCH_POST = 'search.post';
    
    protected $params = array();

    public function setParams(array $params) {
        $this->params = $params;
    }

    public function getParams() {
        return $this->params;
    }
    
    public function search($query, $index) {
        $this->events()->trigger(self::EVENT_SEARCH_PRE, $this);
        $result = $this->_search($query, $index);
        $this->events()->trigger(self::EVENT_SEARCH_POST, $this);
        return $result;
    }
    
    abstract function preSearch();
    abstract function postSearch();
    abstract function _search($query, $index);

}

?>
