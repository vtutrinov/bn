<?php
namespace BNSearch\Service\Strategy;

interface SearchStrategyBehavior {

    public function search($query, $index);
    public function setParams(array $params);
    public function getParams();

}

?>
