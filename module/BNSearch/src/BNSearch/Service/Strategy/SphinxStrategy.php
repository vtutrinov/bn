<?php
namespace BNSearch\Service\Strategy;

class SphinxStrategy extends AbstractStrategy {

    protected $adapter = null;
    
    public function _search($query, $index) {
        $queryOptions = $this->getParams()['query_options'];
        $optionsStr = [];
        array_walk($queryOptions, function($val, $key) use(&$optionsStr) {
            $optionsStr[] = $key.'='.$val;
        });
        $sql = 'select * from '.$index.' where MATCH(\'*'.$query.'*\') option '.implode(',', $optionsStr);
        $results = $this->getAdapter()->getDriver()->getConnection()->execute($sql);
        /*@var $collection \BNSearch\Entity\SearchResultSet */
        $collection = $this->getServiceLocator()->get('SearchServiceFactory')->newResultSet();
        foreach ($results as $item) {
            /*@var $resultItem \BNSearch\Entity\AbstractSearchResultItem  */
            $resultItem = $this->getServiceLocator()->get('SearchServiceFactory')->newResultItem();
            $resultItem->setId(intval($item['id']));
            $resultItem->setName($item['priceitemname']);
            $collection->add($resultItem, $resultItem->getId());
        }
        return $collection;
    }

    public function postSearch() {
        return function($ev) {
            /**
             * @todo unset connection or some GC actions
             */
        };
    }

    public function preSearch() {
        return function($ev) {
            /*@var $target BNSearch\Service\Strategy\SphinxStrategy */
            $target = $ev->getTarget();
            $connectionParams = $target->getParams()['connection'];
            $adapter = new \Zend\Db\Adapter\Adapter($connectionParams);
            $target->setAdapter($adapter);
            $ev->setTarget($target);
        };
    }
    
    public function getAdapter() {
        return $this->adapter;
    }

    public function setAdapter($adapter) {
        $this->adapter = $adapter;
    }

}


?>
