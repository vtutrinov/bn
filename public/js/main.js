$(function(){

    function formatSelection(data) {
        console.log(data);
        return data.text;
    }
    
    $("#autocomplete").select2({
        width : '100%',
        placeholder: 'Type your search request',
        minimumInputLength:1,
        ajax: {
            url:'/search/',
            dataType:'json',
            data: function(term,page) {
                return {
                    q: term,
                    limit: 10
                }
            },
            results: function(data,page) {
                return {
                    results : data.products,
                    more : false
                };
            }
        },
        formatResult: function(data) {
            return '<div class="select2-user-result">' + data.text + '</div>';
        },
//        formatSelection: formatSelection,
        dropdownCssClass: "bigdrop"
    });
    
});
