<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"></meta>
        <meta http-equiv="pragma" content="no-cache"></meta>
        <link rel="stylesheet" href="/css/style.css" />
        <link rel="stylesheet" href="/css/bootstrap-theme.min.css" />
        <link rel="stylesheet" href="/css/bootstrap.min.css" />
        <link rel="stylesheet" href="/js/select2-3.4.5/select2.css" />
        <link rel="stylesheet" href="/js/select2-3.4.5/select2-bootstrap.css" />
        <script type="text/javascript" src="/js/jquery.min.js"></script>
        <script type="text/javascript" src="/js/select2-3.4.5/select2.min.js"></script>
        <script type="text/javascript" src="/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/js/main.js"></script>
    </head>
    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
            </div>
        </nav>
        <div class="container">
            {$content}
            <hr>
            <footer>
                
            </footer>
        </div> <!-- /container -->
    </body>
</html>
