<?php

namespace BN\EventManager;

use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;

trait ProvidesEvents {

	/**
	 *
	 * @var EventManagerInterface
	 */
	private $eventManager = null;

	/**
	 *
	 * @param EventManagerInterface $eventManager        	
	 */
	public function setEventManager(EventManagerInterface $eventManager) {
		$eventManager->setIdentifiers([
			get_called_class() 
		]);
		$this->eventManager = $eventManager;
	}

	/**
	 *
	 * @return \Zend\EventManager\EventManager
	 */
	public function getEventManager() {
		if (null === $this->eventManager) {
			$this->setEventManager(new EventManager());
		}
		return $this->eventManager;
	}

	/**
	 *
	 * @return \Zend\EventManager\EventManager
	 */
	public function events() {
		return $this->getEventManager();
	}

}

?>