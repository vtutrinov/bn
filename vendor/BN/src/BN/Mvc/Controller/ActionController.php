<?php

namespace BN\Mvc\Controller;

use Zend\Mvc\Controller\AbstractActionController;

/**
 * Description of ActionController
 *
 * @author slava
 */
abstract class ActionController extends AbstractActionController {

    public function preDispatch(\Zend\Mvc\MvcEvent $event) {
        //Called before action
    }

    public function postDispatch(\Zend\Mvc\MvcEvent $event) {
        //Called after action
    }

    public function attachDefaultListeners() {
        parent::attachDefaultListeners();
        $events = $this->getEventManager();
        $events->attach(\Zend\Mvc\MvcEvent::EVENT_DISPATCH, array($this, 'preDispatch'), 100);
        $events->attach(\Zend\Mvc\MvcEvent::EVENT_DISPATCH, array($this, 'postDispatch'), -100);
    }

}

?>
