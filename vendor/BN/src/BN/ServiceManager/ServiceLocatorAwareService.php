<?php
namespace BN\ServiceManager;

use Zend\ServiceManager\ServiceLocatorAwareInterface;

/**
 * Description of ServiceLocatorAwareService
 *
 * @author slava
 */
class ServiceLocatorAwareService implements ServiceLocatorAwareInterface {
    
    protected $serviceLocator = null;
    
    public function getServiceLocator() {
        return $this->serviceLocator;
    }

    public function setServiceLocator(\Zend\ServiceManager\ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
    }
    
}

?>
