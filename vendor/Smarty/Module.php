<?php

namespace Smarty;

use Zend\Mvc\MvcEvent;

use Zend\EventManager\StaticEventManager;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;

class Module implements AutoloaderProviderInterface {
	
	public function init() {
		$events = StaticEventManager::getInstance();
		$events->attach('Zend\Mvc\Application', MvcEvent::EVENT_BOOTSTRAP, function($e) {
			$sm = $e->getApplication()->getServiceManager();
			$view = $sm->get('Zend\View\View');
			$strategy = $sm->get('Smarty\View\Strategy\SmartyStrategy');
			$view->getEventManager()->attach($strategy, 100);
			
			$config = $sm->get('Config');
			$renderer = $strategy->getRenderer();
			$engine = $renderer->getEngine();
			if (isset($config['smarty_settings']) && is_array($config['smarty_settings'])) {
				foreach ($config['smarty_settings'] as $param => $value) {
					if (!property_exists(get_class($engine), $param)) {
						throw new \InvalidArgumentException('Invalid smarty settings parameter named \''.$param.'\'');
					}
					$engine->$param = $value;
				}
			}
		});
	}

	public function getAutoloaderConfig() {
		return array(
			'Zend\Loader\ClassMapAutoloader' => array(
				__DIR__ . '/autoload_classmap.php' 
			)
		);
	}

	public function getConfig() {
		return include __DIR__ . '/config/module.config.php';
	}

}
