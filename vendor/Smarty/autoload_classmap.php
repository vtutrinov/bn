<?php
return array(
	'Smarty' => __DIR__ . '/lib/Smarty.class.php',
	'SmartyBC' => __DIR__ . '/lib/SmartyBC.class.php',
	'Smarty\Module' => __DIR__ . '/Module.php',
	'Smarty\View\Renderer\SmartyRenderer' => __DIR__.'/src/Smarty/View/Renderer/SmartyRenderer.php',
	'Smarty\View\Strategy\SmartyStrategy' => __DIR__.'/src/Smarty/View/Strategy/SmartyStrategy.php'
);
