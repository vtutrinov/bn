<?php
/**
 * Специальная функция для отображения даты акций
 *
 * @param $params
 * @return string
 */
function smarty_function_action_date($params)
{
	if (empty($params['begin']) || empty($params['end'])) {
		return 'акция завершена';
	}

	$startDate = strtotime($params['begin']);
	$finishDate = strtotime($params['end']);
	$current = time();
	$tomorrow = strtotime("+ 1 day");

	$months = array(
		'-',
		'января',
		'февраля',
		'марта',
		'апреля',
		'мая',
		'июня',
		'июля',
		'августа',
		'сентября',
		'октября',
		'ноября',
		'декабря',
	);

	if ($current > $startDate && $current < $finishDate) {
		if ($tomorrow >= $finishDate) {

			return 'только сегодня!';
		} else {

			return 'до ' . date("d", $finishDate) . ' ' . $months[date("n", $finishDate)];
		}
	} elseif ($current > $finishDate) {

		return 'акция завершена';
	} else {

		return 'с ' . date("d", $startDate) . ' ' . $months[date("n", $startDate)];
	}
}
