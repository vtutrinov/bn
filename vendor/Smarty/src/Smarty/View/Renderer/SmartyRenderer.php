<?php

namespace Smarty\View\Renderer;

use Zend\View\Renderer\PhpRenderer;
use Zend\View\Exception;
use Zend\View\Model\ModelInterface as Model;

final class SmartyRenderer extends PhpRenderer {

	private $_smarty;

	public function init() {
		$this->_smarty = new \Smarty();
	}

	public function getEngine() {
		return $this->_smarty;
	}

	public function render($nameOrModel, $values = null) {
		$template_path = $nameOrModel;
		
		if ($nameOrModel instanceof Model) {
			$model = $nameOrModel;
			$template_path = $model->getTemplate();
			if (false === strpos($template_path, '.tpl')) {
				$template_path .= '.tpl';
			} 
			if (empty($nameOrModel)) {
				throw new Exception\DomainException(sprintf('%s: received View Model argument, but template is empty', __METHOD__));
			}
			$options = $model->getOptions();
			foreach ($options as $setting => $value) {
				$method = 'set' . $setting;
				if (method_exists($this, $method)) {
					$this->$method($value);
				}
				unset($method, $setting, $value);
			}
			unset($options);
			
			// Give view model awareness via ViewModel helper
			$helper = $this->plugin('view_model');
			$helper->setCurrent($model);
			
			$values = $model->getVariables();
			unset($model);
		}
		
		if (null !== $values) {
			$this->setVars($values);
		}
		unset($values);
		
		$__vars = $this->vars()->getArrayCopy();
		//$__vars['this'] = $this;
		$this->_smarty->assign($__vars);
		
		$content = $this->_smarty->fetch($template_path);
		return $this->getFilterChain()->filter($content); // filter output
	}

}

?>