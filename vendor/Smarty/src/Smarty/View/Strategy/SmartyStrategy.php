<?php
namespace Smarty\View\Strategy;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\View\ViewEvent;
use Zend\EventManager\EventManagerInterface;
use Smarty\View\Renderer\SmartyRenderer;

final class SmartyStrategy implements ListenerAggregateInterface {

	private $_renderer;

	private $_listeners = array();

	public function __construct(SmartyRenderer $renderer) {
		$this->_renderer = $renderer;
	}

	public function attach(EventManagerInterface $events, $priority = null) {
		if (null === $priority) {
			$this->_listeners[] = $events->attach('renderer', array(
				$this,
				'selectRenderer' 
			));
			$this->_listeners[] = $events->attach('response', array(
				$this,
				'injectResponse' 
			));
		} else {
			$this->_listeners[] = $events->attach('renderer', array(
				$this,
				'selectRenderer' 
			), $priority);
			$this->_listeners[] = $events->attach('response', array(
				$this,
				'injectResponse' 
			), $priority);
		}
	}

	public function detach(EventManagerInterface $events) {
		foreach ($this->_listeners as $index => $listener) {
			if ($events->detach($listener)) {
				unset($this->_listeners[$index]);
			}
		}
	}

	public function getRenderer() {
		return $this->_renderer;
	}

	public function selectRenderer(ViewEvent $e) {
		return $this->_renderer;
	}

	public function injectResponse(ViewEvent $e) {
		$renderer = $e->getRenderer();
		if ($renderer !== $this->_renderer) {
			return;
		}
		$result = $e->getResult();
		$response = $e->getResponse();
		$response->setContent($result);
	}
}

?>